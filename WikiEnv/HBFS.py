# heuristic breadth-first search

import wikitools

#############################
# helper functions
def shuffled(L):
    """
    Returns a shuffled version of the given list.
    """

    import random
    L2 = L[:]
    random.shuffle(L2)
    return L2

#############################
# cutoff decision functions
const_cutoff_gen = lambda c: (lambda n:c)
cutoff_10 = const_cutoff_gen(10)

cutoff_5percent = lambda n: int(round(0.05 * n))
#############################

#############################
# HEURISTICS
# (don't require a lookup)
def heuristic_original_order(current_title, goal_title, options_titles):
    """Returns the titles in their original order."""
    return options_titles

def heuristic_title_length(current_title, goal_title, options_titles):
    """Orders the titles by length (shortest first)."""
    return sorted(options_titles, key=lambda t: len(t))

def heuristic_word_in_title(current_title, goal_title, options_titles):
    """Returns the titles ordered by whether the goal_title is in them (True first)."""
    return sorted(options_titles, key=lambda t: goal_title.lower() not in t.lower())

def heuristic_randomized(current_title, goal_title, options_titles):
    """Returns the titles in a randomized order."""
    return shuffled(options_titles)

# (require ONE lookup)
def heuristic_related_term_in_title(current_title, goal_title, options_titles):
    """Returns the titles ordered by whether they can be found in any of the
    terms found on the goal_title's Wiki page (True first)."""

    if '_related_terms' not in globals():
        globals()['_related_terms'] = ' '.join(wikitools.links_from(goal_title))

    related_terms = globals()['_related_terms']

    return sorted(options_titles, key=lambda t: t not in related_terms)

# (require a lookup every time)
def heuristic_number_of_links(current_title, goal_title, options_titles):
    """Orders the titles by the number of links they have (more first)."""

    options_titles = shuffled(options_titles)[:20] # DON'T GET CRAZY
    options_with_pages = map(wikitools.try_get_page, options_titles)
    options_with_pages = filter(lambda p: bool(p), options_with_pages)
    return list(reversed([wikitools.ASCII_PLEASE(p.title) for p in sorted(options_with_pages, key=lambda p: -len(p.links))]))

def heuristic_number_of_references(current_title, goal_title, options_titles):
    """Orders the titles by the number of outbound references they have (more first)."""
    options_titles = shuffled(options_titles)[:20] # DON'T GET CRAZY
    options_with_pages = map(wikitools.try_get_page, options_titles)
    options_with_pages = filter(lambda p: bool(p), options_with_pages)
    return list(reversed([wikitools.ASCII_PLEASE(p.title) for p in sorted(options_with_pages, key=lambda p: -len(p.references))]))
#############################


def HEURISTIC_BFS(current_title, goal_title, options_titles, heuristic, cutoff_func):
    """
    Returns 'cutoff_func(n)' suggestions of items to selectively search next with BFS,
    where n is the original total number of links from the Wikipedia page (usually 10).

    Applies the 'heuristic' function to the list to reorder it, putting the best
    options first.  This abstraction allows various heuristics to be applied.  See above
    for examples.
    """

    options_titles = heuristic(current_title, goal_title, options_titles)
    return options_titles[:cutoff_func(len(options_titles))]


def HBFS_ROUND(current_title, goal_title, heuristic, cutoff_func):
    """
    Performs one "round" of Breadth-First Search, applying the given heuristic and
    cutoff function to selectively rank and visit a subset of all available links.
    """

    options_titles = wikitools.links_from(current_title)

    if not options_titles:
        return []

    if goal_title in options_titles:
        return [goal_title]

    return HEURISTIC_BFS(current_title, goal_title, options_titles, heuristic, cutoff_func)


def HBFS_MAIN(start_title, goal_title, heuristic, cutoff_func):
    """
    Given a starting title and a goal title, a heuristic and cutoff function,
    performs (infinite) rounds of Heuristic-BFS until a path to the goal article is found.
    
    The path is then returned, using the 'history' function and a preserved dictionary of
    child-parent article relationships 'parents'.
    """

    current_title = start_title

    parents = {
        current_title : None,
    }

    def history(title, parents):
        """
        Backtraces through the parent-child dictionary provided
        to find the path taken to the child 'title'.
        """

        if title not in parents:
            return []
        return [title] + history(parents[title], parents)

    if current_title == goal_title:
        print 'special'
        return [current_title]

    next_titles = [current_title]

    keep_going = True
    while keep_going:
        print '-'*30, 'kept going'
        print '::', next_titles

        temp_next_titles = []

        for t in next_titles:
            print t

            # only consider if it's new
            temp_next_titles_t = [newt for newt in HBFS_ROUND(t, goal_title, heuristic, cutoff_func) if newt not in parents]

            # add to database
            for child in temp_next_titles_t:
                if child == t: # don't let a parent point to itself
                    continue
                parents[child] = t

            if goal_title in temp_next_titles_t:
                keep_going = False
                print parents
                return list(reversed(history(goal_title, parents))) # found our result

            temp_next_titles += temp_next_titles_t

        next_titles = temp_next_titles # prepare next layer of results

#######################################

HEURISTIC = heuristic_related_term_in_title
print HEURISTIC.__name__

import time
t0 = time.time()
path = HBFS_MAIN('Kevin Bacon', 'Hitler', HEURISTIC, cutoff_10)
t1 = time.time()

print '-'*80
print "RESULTS:"
print "(Heuristic: {0})".format(HEURISTIC.__name__)
print "Path: {0}".format(str(path))
print "Length: {0}".format(len(path))
print "Time taken: {0}".format(t1 - t0)
