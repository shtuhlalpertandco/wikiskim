# -*- coding: utf-8 -*-

import wikitools


def trunc(s, l, end="..."):
    """
    SEMI-SOURCE: http://stackoverflow.com/questions/2872512/python-truncate-a-long-string

    Pretty-truncates the given string to the given length.
    """

    assert l > len(end)

    if len(s) <= l:
        result = s
    else:
        newl = l - len(end)
        result = s[:newl] + (s[newl:] and end)

    assert len(result) <= l
    return result


# title = wikitools.random_title()
# links = wikitools.links_from(title)[:10]

# print
# print "Manual example:"
# print title
# print links

def sub_cycle(title=None, visited=None):
    """
    Performs a single cycle of looking up a title and getting its links;
    finds a similar suggestion if can't find given title, or if none given.
    """

    # guarantee a title
    if not title:
        title = wikitools.random_title()

    # guarantee a unique title
    if visited:
        while title in visited:
            title = wikitools.random_title()


    # in the COMMON case of an ambiguous title
    try:
        links = wikitools.links_from(title)
    # in the RARE case of the above ^ not fixing it...
    except wikipedia.exceptions.DisambiguationError: # cute.
        title = wikitools.clarify(title)

        if not title:
            return (None, None) # there was a disambiguation error... but no suggestion?  doubtful.
        else:
            try:
                links = wikitools.links_from(title)
            except:
                return (None, None)
    except:
        print "Oh just stop it."
        return (None, None) # also doubtful.

    return (title, links)

# print
# print "Automated (sub-)example:"
# print sub_cycle()

def cycle(i=3, title=None, visited=None, pending=None):
    """
    Performs 'i' cycles of 'sub_cycle', managing sets of visited and pending titles,
    to perform quasi-depth-first-search.

    Manages and returns a large JSON-like dictionary record of articles and links to store.
    """

    # guarantee visited
    if not visited:
        visited = set()

    # guarantee pending
    if not pending:
        pending = set()

    mapped_dict = {}

    for j in xrange(i):
        _saved_title = title
        (title, links) = sub_cycle(title, visited) # the set of visited is (maybe?) redundant when 'mapped_dict' is here

        # special case where we didn't get a title...
        if not title:
            print "No output from {0}!".format(_saved_title)
            title = wikitools.random_title() # only time we ever intervene...
            continue
        else:
            assert links
        
        # update our "database" JSON
        if title not in mapped_dict:
            mapped_dict[title] = links # unfiltered links
        else:
            print "Visited {0} already.  Hmmm...".format(title)

        visited.add(title)
        print "(just visited {0})".format(title)
        pending = pending.union(set(links)) # update pending set
        pending.symmetric_difference_update(visited) # always prune visited nodes.  don't waste time.

        # get next node!
        if pending:
            title = wikitools.random_from_set(pending)
            pending.remove(title)

    return (mapped_dict, pending)


def main():

    print
    print "Automated example:"
    (JSON, pending) = cycle(i=20)

    print
    print "Articles and links:"
    for (k,v) in JSON.iteritems():
        print "{0} ({2} links): {1}".format(k, trunc(str(v), 50), len(v))



if __name__ == '__main__':
    main()
    pass

