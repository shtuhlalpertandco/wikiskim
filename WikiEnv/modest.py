
# standard lib
import time

# custom lib
from tinydb import TinyDB, where

# personal lib
import main


HARD_WAIT_TIME = 60
TOTAL_RUN_CYCLES = 100 # < I don't want to literally fill my harddrive.  Copying all of wikipedia isn't my goal (yet).

size_per_iter = 10
iter_per_batch = 5


def save_uniques(db, json):
    """
    Saves the unique (read: not in the database) elements from the given JSON
    to the given database 'db'.
    """

    for (k,v) in json.iteritems():
        result = db.search(where('title') == k)
        if not result:
            db.insert({'title':k, 'links':v})
        else:
            print "Duplicate in DB already: {0}".format(result)

def dumb_batch(size, count, wait):
    """
    Runs 'count' counts of main.cycle batches of random article lookups
    (for database population/testing).  Each batch runs 'size' deep.

    Waits reasonable amounts of time between batches so as not to harass Wikipedia
    (designed to be run overnight, after all.)

    Saves the results to a TinyDB database.
    """

    db = TinyDB('db.json')

    pending = set()

    print "Running {0} batches of {1} entries each...".format(count, size)
    for i in xrange(count):
        (new_dict, pending) = main.cycle(i=size, pending=pending)

        save_uniques(db, new_dict)

        print "(Waiting {0} seconds...)".format(wait)
        time.sleep(wait)

    # (mostly in spirit.  this would've happened anyways.)
    # (keep the RAM low and slow)
    del db # this flushes RAM to the drive (equivalent to the non-existent 'db.flush()' or 'db.close()')
    del pending
    del new_dict

print "!! NOTE !!"
print ">> About to run {0} batches of {1}x{2}={3} requests each (that's {4} requests total).  These generate a LOT of data (albeit slowly). <<".format(TOTAL_RUN_CYCLES, size_per_iter, iter_per_batch, size_per_iter * iter_per_batch, size_per_iter * iter_per_batch * TOTAL_RUN_CYCLES)
print "!! YOU HAVE BEEN WARNED !!"
time.sleep(15)

for i in xrange(TOTAL_RUN_CYCLES):
    dumb_batch(size_per_iter, iter_per_batch, 5)
    print "[ BATCH COMPLETE ]"
    print "[ WAITING {0} SECONDS OUT OF POLITENESS TO WIKIPEDIA ]".format(HARD_WAIT_TIME)
    time.sleep(HARD_WAIT_TIME)