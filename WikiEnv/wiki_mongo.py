from pymongo import MongoClient

# init the mongo client. Make sure mongod is running first.
client = MongoClient('mongodb://localhost:27017')

# use the wikipedia database
db = client.wikipedia

# the document collection in the database
collection = db.documents

""" IM SO BUMMED I COULDN'T OVERLOAD THESE TWO FUNCTIONS"""


# add a document that has not yet been crawled to the database.
# title is a string, the title of the document.
def add_empty_document(title):
    # we only want to add a blank entry for a doc if it's not already there.
    # luckily mongo indexes by _id so this is constant time.
    if not collection.findOne({"_id": title}):
        collection.insert({"_id": title, "links": []})


# add a document that has been crawled to the database.
# title is a string, the title of the document.
# links is a list of titles this document points to.
def add_document(title, links):
    # we upsert this document, because even if it exists we want to update the links.
    # implementations using this module should only ever run this command once per document.
    collection.update({"_id": title}, {"links": links}, upsert=True)


# find a document that needs to be crawled
# returns dict of form {'_id': [String]doc_title, 'links': [List]linked_docs}
def find_empty_document():
    return collection.findOne({"links": []})


# find a specific document
# title is a string, the name of the document to return
def find_document(title):
    cursor = collection.find({"_id": title})
    return cursor.next()